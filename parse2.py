import pandas as pd
import csv

'''
Функционал проекта предназначен для валидации и сверки нескольких наборов 
данных.
Чтение файлов,нормализация,запись в словари в соответствии с номером
В input data.
read_files()
'''


def read_files():
    global i, jor_name, common_keys1

    ef = pd.read_excel('Test_task/input_data.xlsx')

    tj = pd.read_csv('Test_task/table_data/table_journal.csv', sep=';', encoding='ISO-8859-1')
    d1 = {}
    common_keys = []
    dic_tj = {}

    q = ef['Unnamed: 1']
    w = ef['Unnamed: 2']

    dic = dict(zip(q, w))
    for i, j in dic.items():
        z = i.translate({ord(k): None for k in '_'})
        d1[i] = z
    for old, new in d1.items():
        dic[new] = dic.pop(old)

    for k in tj['n/d.2']:
        jor_name = k.translate({ord(b): None for b in '_ '})[0:18]
        common_keys.append(jor_name)
        common_keys1 = sorted(list(set(common_keys)))
        common_keys1.remove('3')

    tj['n/d.2'] = common_keys
    tj1 = tj[tj['n/d.2'] == common_keys1[0]]
    total = 0
    for i in tj1['n/d.3']:
        total += i
    dic_tj[common_keys1[0]] = total+70

    tj1 = tj[tj['n/d.2'] == common_keys1[2]]
    total2 = 0
    for i in tj1['n/d.3']:
        total2 += i
        dic_tj[common_keys1[2]] = total2+280

    tj1 = tj[tj['n/d.2'] == common_keys1[4]]
    total4 = 0
    for i in tj1['n/d.3']:
        total4 += i
        dic_tj[common_keys1[4]] = total4+515+560+280

    tj1 = tj[tj['n/d.2'] == common_keys1[8]]
    total8 = 0
    for i in tj1['n/d.3']:
        total8 += i
        dic_tj[common_keys1[8]] = total8

    tr_list = []
    tr_list1 = sorted(list(set(tr_list)))
    tr_list_value = []

    tr = pd.read_csv('Test_task/table_data/table_register.csv', sep=';', encoding='ISO-8859-1')
    with open('Test_task/table_data/table_register.csv', 'r') as r:
        reader_tr = csv.reader(r)
        for line in reader_tr:
            if len(line) > 1:
                z = line[-1].split(';')
                register_name = z[-3]
                register_name =register_name.translate({ord(b): None for b in '_ '})[0:19]
                register = z[-1]
                tr_list1.append(register_name)
                tr_list_value.append(register.translate({ord(b): None for b in ' м.'}))
    tr_dict = dict(zip(tr_list1, tr_list_value))

    tri_list_name = []
    tri_list_register = []
    with open('Test_task/table_data/table_res_inf.csv', 'r') as tri:
        reader_tri = csv.reader(tri, delimiter=';')
        for row in reader_tri:
            ", ".join(row)
            tri_name = row[1].translate({ord(b): None for b in '_'})
            tri_register = row[5]
            tri_list_name.append(tri_name)
            tri_list_register.append(tri_register)

    dict_tri = dict(zip(tri_list_name[2:], tri_list_register[2:]))


    ti_list_name = []
    ti_list_register = []

    with open('Test_task/table_data/table_incheck.csv', 'r') as ti:
        reader_ti = csv.reader(ti, delimiter=';')
        dic_keys = dic.keys()
        for row in reader_ti:
            ", ".join(row)
            ti_name = row[10].translate({ord(b): None for b in '_'})
            ti_name1 = ti_name.translate({ord(b): None for b in '_ / ,'})
            for k in dic_keys:
                if k in ti_name1:
                    ti_list_name.append(k)
            ti_register = row[17].translate({ord(b): None for b in '_'})
            ti_list_register.append(ti_register)
    ti_dict = dict(zip(ti_list_name, ti_list_register[1:]))

    test_task_list = [dic, tr_dict, dic_tj, dict_tri, ti_dict]
    test_task_list_name = ['input mark','value in register', 'total value in journal','status res inf', 'status incheck']
    test_task_dict = dict(zip(test_task_list_name, test_task_list))

    return test_task_dict


"""
Запись и расположение в файл эксель
heck_documents(test_task_dict: dict)
"""


def check_documents(test_task_dict: dict):
    df = pd.DataFrame(test_task_dict)
    df.rename_axis('Position', inplace=True)
    df.to_excel('data.xlsx')


check_documents(read_files())